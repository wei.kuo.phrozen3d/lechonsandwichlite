#include "Common.h"
#include <string>

class CBenanoSDKExample;
class LechonController;

/* [June 4, 2021]
*  Need to define the deviceInfo and liveControl APIs later,
*  currently, it is failed to link Benano objects by reference.
*/
//class CDeviceInfo;
//class CLiveCtrl;

//class LECHON_PLATE_API LechonPlateDeviceInfo {
//
//
//};

//class LECHON_PLATE_API LechonPlateLiveController {
//
//};

class LechonPlateIntegrator {

public:
    LechonPlateIntegrator(void);
    ~LechonPlateIntegrator(void);

public:

	// Conncet to scanner.
	bool InitializeScanner();

	/* [June 4, 2021]
	*  If we add deviceInfo and liveControl inside integrator,
	*  seems these two function could be done inside the initialization.
	*/
	bool CheckDeviceInfo();
	bool CheckLiveController();

	// Get live 2D images.
	bool SnapLiveImage(const std::string outputFileDirectory);
	
	// Get rgbd images and point clouds.
	bool StartScan();
	bool StopScan();

	// Output result
	void WriteOneViewResultColorImage(const std::string outputFileDirectory);
	void WriteOneViewResultDepthImage(const std::string outputFileDirectory);
	void WriteOneViewResultPointCloud(const std::string outputFileDirectory);

private:
	CBenanoSDKExample* m_Scanner;
	LechonController* m_Controller;

private:
	//CDeviceInfo* m_deviceInfo;
	//CLiveCtrl* m_LiveController;
};
