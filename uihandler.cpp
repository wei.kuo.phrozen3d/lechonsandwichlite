#include "uihandler.h"
#include <QDebug>

UIHandler::UIHandler(const QString& ap_path)
{
    qDebug()<<"UIHandle init :"<<ap_path;
    m_ap_path = ap_path;
    m_first_line ="Initialzing";
    m_second_line="Hello Sandwich!!";
    m_third_line ="";
}

void UIHandler::setFirstLine(const QString &first_line)
{
    //ss
    m_first_line = first_line;
}

void UIHandler::setSecondLine(const QString &second_line)
{
    //ss
    m_second_line = second_line;
}

void UIHandler::setThirdLine(const QString &third_line)
{
    //ss
    m_third_line = third_line;
}

void UIHandler::buttonDeviceInfo()
{
    setFirstLine("did not connect with Device");
    setSecondLine("on working...");
    setThirdLine("on working...");
    emit textChanged();
}

void UIHandler::buttonSnapLiveImage()
{
    QString temp = "store in : "+m_ap_path+"/file";
    setFirstLine("you press snap live image");
    setSecondLine(temp);
    setThirdLine("it is failed");
    emit textChanged();
}

void UIHandler::buttonStartScan()
{
    //QString temp = "store in : "+m_ap_path+"/file";
    setFirstLine("you press start scan");
    setSecondLine("");
    setThirdLine("it is failed");
    emit textChanged();
}

void UIHandler::buttonSaveRGB()
{
    QString temp = "store in : "+m_ap_path+"/file";
    setFirstLine("you press save RGB");
    setSecondLine(temp);
    setThirdLine("it is failed");
    emit textChanged();
}

void UIHandler::buttonSaveDepth()
{
    QString temp = "store in : "+m_ap_path+"/file";
    setFirstLine("you press save Depth");
    setSecondLine(temp);
    setThirdLine("it is failed");
    emit textChanged();
}

void UIHandler::buttonSavePointCloud()
{
    QString temp = "store in : "+m_ap_path+"/file";
    setFirstLine("you press save point cloud");
    setSecondLine(temp);
    setThirdLine("it is failed");
    emit textChanged();
}
