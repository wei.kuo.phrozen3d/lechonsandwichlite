#ifndef UIHANDLER_H
#define UIHANDLER_H
#include <QObject>
#include <QDir>

class UIHandler : public QObject
{
    Q_OBJECT
public:
    UIHandler(const QString& ap_path);


    Q_PROPERTY(QString first_line READ getFirstLine  WRITE setFirstLine  NOTIFY textChanged)
    QString getFirstLine()
    {
        return m_first_line;
    }

    Q_PROPERTY(QString second_line READ getSecondLine  WRITE setSecondLine  NOTIFY textChanged)
    QString getSecondLine()
    {
        return m_second_line;
    }

    Q_PROPERTY(QString third_line READ getThirdLine  WRITE setThirdLine  NOTIFY textChanged)
    QString getThirdLine()
    {
        return m_third_line;
    }


    Q_INVOKABLE void buttonDeviceInfo();
    Q_INVOKABLE void buttonSnapLiveImage();
    Q_INVOKABLE void buttonStartScan();
    Q_INVOKABLE void buttonSaveRGB();
    Q_INVOKABLE void buttonSaveDepth();
    Q_INVOKABLE void buttonSavePointCloud();

signals:
    void textChanged();

protected:
    void setFirstLine(const QString& first_line);
    void setSecondLine(const QString& second_line);
    void setThirdLine(const QString& third_line);

private:
    QString m_ap_path;
    QString m_first_line;
    QString m_second_line;
    QString m_third_line;

};

#endif // UIHANDLER_H
