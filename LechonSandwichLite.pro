QT += quick qml

CONFIG += c++11
TEMPLATE = app
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        apiinterface.cpp \
        main.cpp \
        uihandler.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    apiinterface.h \
    uihandler.h

#LIBS += -L$$PWD/lib/ -lLechonPlateRelease
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ -lLechonPlateReleased

INCLUDEPATH += $$PWD/lib \
                $$PWD/lib/header

#LIBS += $$PWD/lib/LechonPlateRelease.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/LechonPlateReleased.lib

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/ -lLechonPlateRelease
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ -lLechonPlateReleased

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/libLechonPlateRelease.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/libLechonPlateReleased.a
win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/LechonPlateRelease.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/LechonPlateReleased.lib
