import QtQuick 2.12
import QtQuick.Window 2.12

import "Base"

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Lechon Sandwich Lite")

    property int button_width: 150
    property int button_height: 30
    property int text_size: 15

    property string first_line: ui.first_line
    property string second_line: ui.second_line
    property string third_line: ui.third_line


    ButtonBase{
        id:device_info
        anchors.top:snap_live_image.top
        anchors.right:snap_live_image.left
        anchors.rightMargin: 30
        width:button_width
        height:button_height
        _border_width:1
        _text:"Device Info"
        onClicked: ui.buttonDeviceInfo()
    }

    ButtonBase{
        id:snap_live_image
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top:parent.top
        anchors.topMargin: 30
        width:button_width
        height:button_height
        _border_width:1
        _text:"Snap Live Image"
        onClicked: ui.buttonSnapLiveImage()
    }

    ButtonBase{
        id:start_scan
        anchors.top:snap_live_image.top
        anchors.left:snap_live_image.right
        anchors.leftMargin: 30
        width:button_width
        height:button_height
        _border_width:1
        _text:"Start Scan"
        onClicked: ui.buttonStartScan()
    }

    //
    ButtonBase{
        id:save_rgb
        anchors.top:save_depth.top
        anchors.right:save_depth.left
        anchors.rightMargin: 30
        width:button_width
        height:button_height
        _border_width:1
        _text:"Save RGB"
        onClicked: ui.buttonSaveRGB()
    }

    ButtonBase{
        id:save_depth
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top:snap_live_image.bottom
        anchors.topMargin: 30
        width:button_width
        height:button_height
        _border_width:1
        _text:"Save Depth"
        onClicked: ui.buttonSaveDepth()
    }

    ButtonBase{
        id:save_point_cloud
        anchors.top:save_depth.top
        anchors.left:save_depth.right
        anchors.leftMargin: 30
        width:button_width
        height:button_height
        _border_width:1
        _text:"Save Point Cloud"
        onClicked: ui.buttonSavePointCloud()
    }

    ButtonBase{
        id:save_mesh
        anchors.top:save_rgb.bottom
        anchors.topMargin: 30
        anchors.right:save_rgb.right
        //anchors.rightMargin: 30
        width:button_width
        height:button_height
        opacity: 0.3
        _border_width:1
        _text:"Save Mesh"
        MouseArea{
            anchors.fill:parent
        }

    }

    Rectangle{
        id:message_output
        anchors.top:save_mesh.bottom
        anchors.topMargin: 30
        anchors.left:save_mesh.left
        width: 530
        height: 250
        color: "transparent"
        border.color: "black"
        border.width: 1
        Text{
            id:firstline_text
            anchors.left:parent.left
            anchors.leftMargin: 5
            font.pixelSize:text_size
            font.family: "Arial"
            color:'black'
            text:">> "+first_line
        }
        Text{
            id:secondline_text
            anchors.top:firstline_text.bottom
            anchors.topMargin: 5
            anchors.left:firstline_text.left
            font.pixelSize:text_size
            font.family: "Arial"
            color:'black'
            text:">> "+second_line
        }
        Text{
            id:thirdline_text
            anchors.top:secondline_text.bottom
            anchors.topMargin: 5
            anchors.left:firstline_text.left
            font.pixelSize:text_size
            font.family: "Arial"
            color:'black'
            text:">> "+third_line
        }
    }
}
