import QtQuick 2.7

Item {
    property string _text : ""
    property int _text_size: 15
    property string _text_color: "white"
    property int _border_width: 0
    property string _rec_color: "transparent"
    property int _rec_radius: 0
    property string _text_layout: "middle"

    Rectangle {
          width: parent.width
          height: parent.height
          color: _rec_color
          border.width:_border_width
          border.color: "red"
          radius: _rec_radius
          Text{
              id:text_root
              anchors.centerIn: parent.Center
              anchors.verticalCenter: parent.verticalCenter
              anchors.horizontalCenter: {
                  if (_text_layout == "left")
                  {
                      return;
                  }

                  if (_text_layout == "middle")
                  {
                      return parent.horizontalCenter;
                  }
              }
              text: _text
              font.pixelSize:_text_size
              color: _text_color
              font.family: "Arial"
              //wrapMode: TextEdit.Wrap
          }
      }
}
