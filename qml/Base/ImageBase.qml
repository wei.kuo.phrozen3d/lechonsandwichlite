import QtQuick 2.7
import "."

Item {
    property string _source : ""
    property int _border_width: 0

    Rectangle {
          id:rec_logo
          width: parent.width
          height: parent.height
          color:"transparent"
          border.color:"red"
          border.width: _border_width

          Image {
             anchors.fill: parent
             source: _source
             sourceSize.width: parent.width
             sourceSize.height: parent.height
          }
          MouseArea{

          }
      }
}
