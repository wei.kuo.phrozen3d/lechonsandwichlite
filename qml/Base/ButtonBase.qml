import QtQuick 2.7

Item {
    id:root
    property string _text : ""
    property int _text_size: 15
    property string _text_color: "black"
    property int _border_width: 0
    property string _rec_color: "transparent"
    property int _rec_radius: 0
    //property int _button_action_type: -1

    signal clicked()

    Rectangle {
          width: parent.width
          height: parent.height
          color: {
              if(button.containsMouse)
              {
                  return "orange"
              }else{
                  return _rec_color;
              }
          }
          border.width:_border_width
          border.color: "black"
          radius: _rec_radius
          Text{
              anchors.centerIn: parent.Center
              anchors.horizontalCenter: parent.horizontalCenter
              anchors.verticalCenter: parent.verticalCenter
              text: _text
              font.pixelSize:_text_size
              color: _text_color
              font.family: "Arial"
          }
          MouseArea{
              id:button
              anchors.fill:parent
              enabled: true
              onClicked: {
                  root.clicked();
                  //ui_handler.uiEvent(_button_action_type);
              }
          }
      }

    Connections{
//        target: ui_handler
//        onSigMessageBoxVisible:{
//            // if messagebox.visible = true , button should lock
//            button.enabled = !_visible;
//        }
    }
}
