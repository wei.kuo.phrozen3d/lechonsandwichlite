#ifndef APIINTERFACE_H
#define APIINTERFACE_H

class LechonPlateIntegrator;
class ApiInterface
{
public:
    ApiInterface();

private:
    LechonPlateIntegrator* m_integrator;
};

#endif // APIINTERFACE_H
